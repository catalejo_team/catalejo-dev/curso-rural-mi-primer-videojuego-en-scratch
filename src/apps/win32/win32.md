# Instalación de apps en windows

- [Scratch 3.0 link de descarga](./scratch-3.24.0-setup.exe): Esta aplicación te permitirá crear el vídeojuego que encuentras en la sesión
de "Mi primer juego con scratch"

- [Sumatra link de descarga](./sumatra-pdf.exe): Sino tienes un lector de pdf, podrás instalar sumatra para leer las sesiones
en formato pdf (sesión1.pdf, sesión2.pdf...)

- [VLC media player link de descarga](./vlc-3.0.16-win32.exe): En todas las sesiones tenemos vídeos que podrás seguir para aprender
sobre temas nuevos, sino tiene un reproductor de audio y vídeos, VLC será tu mejor amigo.

