# Curso de Ciencia, Tecnología y Sociedad 

!Bienvenidas y bienvenidos al curso de *Ciencia, Tecnología y Sociedad*!

<video style="max-width: 100%; height: auto;" controls>
    <source src="./introduccion.mp4" type="video/mp4">
</video> 

Vídeo tomado de youtube link: [https://youtu.be/ZROUG_Ge1y0](https://youtu.be/ZROUG_Ge1y0)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de **curso** como:
>
> `introduccion.mp4`

## ¿Qué haremos?

En este curso aprenderemos acerca de la tecnología, su relación con
nosotras y nosotros y nuestro entorno, viviremos una experiencia de
aprendizajes donde exploraremos a través de nuestros intereses y
motivaciones haciendo una lectura de nuestra realidad para finalmente
proponer un cambio que deseemos en ella.

## ¿Cómo lo haremos?

![fases de ABP](./infografia.jpeg)
.
Usaremos la metodología de *aprendizaje basado en fenómenos*, con la cual
aprenderemos con las manos, es decir, en el hacer y reflexionando sobre
nuestras acciones.

## ¿Cuánto tiempo dura la experiencia?

La experiencia de aprendizaje tendrá un tiempo estimado de `seis (6) semanas`

## ¿Con qué lo haremos?

Necesitaremos lo siguiente:

* Un cuaderno u hojas de papel donde puedas hacer las actividades que te propondremos
* Lápices, colores, marcadores, tijeras, colbón, lo que necesites para dejar volar tu creatividad
* Muchas ganas de aprender y de compartir todo lo que aprendas con nosotros

> **Nota**: También en este material encontrarás aplicaciones que podrás instalar en un
> computador con Windows sin inconvenientes; tales aplicaciones pueden mejorar
> tu experiencia.

## ¿Qué evaluaremos?

Evaluaremos las actividades que realices en tu cuaderno u hojas, para eso, ten
presente que en algún momento ye pediré que me hagas llegar ese material.

Tendrás que realizar una, dos o tres actividades por semana, y la suma de
esas actividades serán las que te permitan aprobar el curso

## ¿El curso será certificado por la Universidad Nacional de Colombia?

La respuesta es sí, pero para eso, tendrás que entregar al menos el 80 %
de las actividades que equivaldrán a la asistencia de las seis semanas, además,
la evaluación de la entrega de las actividades deben ser mayores e iguales a **3.0**

## ¿Cómo entregar las actividades?

Las actividades las entregarás en un cuaderno u hojas de papel las cuales deberán
estar marcadas con:

> * Nombre completo
> * Número del documento de identidad
> * Curso y Colegio
> * Si te es posible poner, número de celular y correo, lo que tengas

## ¿Hay bonus?

Sí, hay unas actividades opcionales que puedes entregar para poder mejorar tu calificación,
las actividades pueden consistir en crear un vídeo juego, crear notas de voz o grabar en vídeo alguna actividad,
te irás dando cuenta en el desarrollo de cada sesión.

¡DIVIERTE Y APRENDE!

Con mucho cariño,

```
Johnny Cubides
Ing Electrónico
Univerdad Nacional de Colombia
jgcubidesc@unal.edu.co
```

[Colaboradores](../../misc/colaboradores.md)

<!-- insert in the document body -->
<!-- <object class="pdfobject-container" --> 
<!-- data='python.pdf#comment=Ejemplo&view=FitB&scrollbar=1&toolbar=1&statusbar=1&navpanes=1' -->
<!--         type='application/pdf' --> 
<!--         width='100%' --> 
<!--         height='100%'> -->
<!-- <p>This browser does not support inline PDFs. Please download the PDF to view it: <a href="python.pdf">Download PDF</a></p> -->
<!-- </object> -->
