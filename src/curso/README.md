# Curso de Ciencia, Tecnología y Sociedad 

!Bienvenidas y bienvenidos al curso de *Ciencia, Tecnología y Sociedad*!

En este curso aprenderemos acerca de la tecnología, su relación con
nosotras y nosotros y nuestro entorno, viviremos una experiencia de
aprendizajes donde exploraremos a través de nuestros intereses y
motivaciones haciendo una lectura de nuestra realidad para finalmente
proponer un cambio que deseemos en ella.

Mira el siguiente vídeo para que puedas comprender como hacer uso de esta documentación.

<video style="max-width: 100%; height: auto;" controls>
    <source src="./bienvenida-docs.mp4" type="video/mp4">
</video> 

Vídeo tomado de youtube link: [https://youtu.be/ZROUG_Ge1y0](https://youtu.be/ZROUG_Ge1y0)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de **curso** como:
>
> `bienvenida-docs.mp4`
