# Sesión 3: La programación como herramienta transversal


Hola, te damos la bienvenida a nuestra primera semana, para que sepas
de qué se trata, dale play al siguiente audio:

<audio controls>
    <source src="./semana-inicio.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

> **Atención**: Sino puedes reproducir el audio, búscalo en la carpeta de la **sesión 3** como:
>
> `semana-inicio.mp3`

## Actividad 1: ¿Por qué aprender programación?

Mira el siguiente vídeo y haz lo siguiente: teniendo en cuenta las posibilidades para solucionar problemas por medio de la programación. Describe situaciones o necesidades del campo que quisieras que mejoraran y cómo pretendes solucionarlas. (Ejemplo: Colocar un aspersor, para regar las plantas más fácil en un tiempo determinado y no malgastar agua). Ahora sigues tú:

> **Nota**: La actividad la puedes desarrollar con la ayuda de un cuaderno, no dejes de lado la creatividad apoyándote con dibujos.

<video style="max-width: 100%; height: auto;" controls>
    <source src="./video1-por-que-programar.mp4" type="video/mp4">
</video> 

Vídeo tomado de youtube link: [https://youtu.be/ZROUG_Ge1y0](https://youtu.be/ZROUG_Ge1y0)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 3** como:
>
> `video1-por-que-programar.mp4`

## Actividad 2: El RAP de programación

<video style="max-width: 100%; height: auto;" controls>
    <source src="./video2-el-rap-de-la-programacion.mp4" type="video/mp4">
</video> 

Vídeo tomado de youtube link: [https://youtu.be/-8EbcJuWOtw](https://youtu.be/-8EbcJuWOtw)

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 3** como:
>
> `video2-el-rap-de-la-programacion.mp4`

Teniendo en cuenta el video de RAP de programación, completa el siguiente crucigrama, escucha atentamente el vídeo:

![crucigrama](crucigrama.jpg)

## Actividad 3: Ayudando a otros

<video style="max-width: 100%; height: auto;" controls>
    <source src="./video3-chica-3d-impresion-gallinas-ayuda.mp4" type="video/mp4">
</video> 

Vídeo tomado de: https://cnnespanol.cnn.com/video/australia-impresora-3d-pollos-patas-es-viral-pm-pkg-digital-original/

> **Atención**: Sino puedes reproducir el vídeo, búscalo en la carpeta de las **sesión 3** como:
>
> `video3-chica-3d-impresion-gallinas-ayuda.mp4`

¿No te sorprende la herramienta que le creó esta niña a la gallina para ayudarla con su discapacidad?
En esta actividad te proponemos varias situaciones con gallinas, para ayudar a sus cuidadores a encontrar una solución.

> Situación 1: A Juana se le han estado perdiendo sus gallinas y sospechan que es un animal el que llega a comérselas.
> Solución 1: Necesita de un dispositivo que le avise cuando ocurra un movimiento en la entrada del corral.

Ahora, usa tu imaginación para dar respuesta a estas situaciones:
> Situación 2: Juan se irá de viaje y no tiene alguien para dejar encargado de la alimentación de sus gallinas.
>
> Solución 2: _______________________________________________

> Situación 3: Samuel se ha dado cuenta que sus gallinas ponedoras no abrigan sus huevos.
>
> Solución 3: _______________________________________________

## Actividad 4: Personajes y entorno del juego

<audio controls>
    <source src="./audio2-instrucciones-juego.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

> **Atención**: Sino puedes reproducir el audio, búscalo en la carpeta de las sesión 3 como:
>
> `audio2-instrucciones-juego.mp3`

En esta actividad plantearás la idea de tu video juego.

> Describe tu personaje principal 
>
> Rta: _______________________________________________

> Describe el entorno del video juego (Ejemplos: bosque, plataforma, ciudad, etc)
>
> Rta: _______________________________________________

> Describe los enemigos
>
> Rta: _______________________________________________

> Objetivo del video juego
>
> Rta: _______________________________________________

> Realiza una narrativa en general
>
> Rta: _______________________________________________

## Actividad 5 (Opcional): Implementación de vídeo juego en scratch

<audio controls>
    <source src="./inicio-de-juego.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

> **Atención**: Sino puedes reproducir el audio, búscalo en la carpeta de la **sesión 3** como:
>
> `inicio-de-juego.mp3`

Ve a los siguientes link y realiza las tareas allí establecidas, empezarás a programar
tu primer vídeo juego

- [Iníciate en Scratch](../../juego-scratch/1.-Abrir-Scratch/tutorial1.md)
- [Guardar proyecto en el computador](../../juego-scratch/8.-Guardar-proyecto-en-el-computador/tutorial8.md)
